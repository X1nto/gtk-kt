package org.gtk.glib

import glib.*
import org.gtk.glib.Variant.Companion.wrap

/**
 * gtk-kt
 *
 * 15 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.VariantIter.html">
 *     GVariantIter</a>
 */
class VariantIter(val pointer: GVariantIter_autoptr) : FreeMe {

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantIter.copy.html">
	 *     g_variant_iter_copy</a>
	 */
	fun copy() =
		g_variant_iter_copy(pointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantIter.free.html">
	 *     g_variant_iter_free</a>
	 */
	override fun free() {
		g_variant_iter_free(pointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantIter.init.html">
	 *     g_variant_iter_init</a>
	 */
	fun init(value: Variant): ULong =
		g_variant_iter_init(pointer, value.variantPointer)

	// TODO loop https://docs.gtk.org/glib/method.VariantIter.loop.html

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantIter.n_children.html">
	 *     g_variant_iter_n_children</a>
	 */
	val nChildren: ULong
		get() = g_variant_iter_n_children(pointer)

	// TODO next https://docs.gtk.org/glib/method.VariantIter.next.html

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.VariantIter.next_value.html">
	 *     g_variant_iter_next_value</a>
	 */
	fun nextValue(): Variant? =
		g_variant_iter_next_value(pointer).wrap()

	companion object {
		inline fun GVariantIter_autoptr?.wrap() =
			this?.wrap()

		inline fun GVariantIter_autoptr.wrap() =
			VariantIter(this)
	}
}