package org.gtk.glib

import glib.GSource_autoptr

/**
 * gtk-kt
 *
 * 29 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.Source.html"></a>
 */
class Source(val pointer: GSource_autoptr) {

	companion object {

		inline fun GSource_autoptr?.wrap() =
			this?.wrap()

		inline fun GSource_autoptr.wrap() =
			Source(this)
	}
}