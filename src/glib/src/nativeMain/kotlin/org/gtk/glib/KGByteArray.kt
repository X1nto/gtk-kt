package org.gtk.glib

import glib.GByteArray_autoptr
import glib.g_byte_array_new
import glib.g_byte_array_unref

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/glib/type_func.ByteArray.ref.html">
 *     GByteArray</a>
 */
class KGByteArray(
	val struct: GByteArray_autoptr,
) : UnrefMe {
	constructor() : this(g_byte_array_new()!!)

	override fun unref() {
		g_byte_array_unref(struct)
	}
}