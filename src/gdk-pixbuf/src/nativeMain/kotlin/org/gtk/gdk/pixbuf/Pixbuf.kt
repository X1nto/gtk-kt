package org.gtk.gdk.pixbuf

import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject
import pixbuf.GdkPixbuf

/**
 * kotlinx-gtk
 * 26 / 03 / 2021
 */
class Pixbuf(val pixbufPointer: CPointer<GdkPixbuf>) : KGObject(pixbufPointer.reinterpret()) {

	companion object {
		fun CPointer<GdkPixbuf>?.wrap() =
			this?.wrap()

		fun CPointer<GdkPixbuf>.wrap() =
			Pixbuf(this)
	}
}