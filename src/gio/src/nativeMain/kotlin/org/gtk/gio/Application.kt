package org.gtk.gio

import gio.*
import glib.GError
import glib.GVariantDict
import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.*
import org.gtk.gio.ApplicationCommandLine.Companion.wrap
import org.gtk.gio.DBusConnection.Companion.wrap
import org.gtk.gio.File.Companion.toCArray
import org.gtk.gio.File.Companion.wrap
import org.gtk.glib.*
import org.gtk.glib.VariantDictionary.Companion.wrap
import org.gtk.gobject.*
import org.gtk.gobject.Signals.ACTIVATE
import org.gtk.gobject.Signals.COMMAND_LINE
import org.gtk.gobject.Signals.HANDLE_LOCAL_OPTIONS

/**
 * 22 / 02 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.Application.html">
 *     GApplication</a>
 */
open class Application(
	private val gApplicationPointer: GApplication_autoptr,
) : KGObject(gApplicationPointer.reinterpret()), ActionMap {
	// TODO g_application_unbind_busy_property

	fun unbindBusyProperty(obj: KGObject, property: String) {
		g_application_unbind_busy_property(
			gApplicationPointer,
			obj.pointer,
			property
		)
	}

	fun bindBusyProperty(obj: KGObject, property: String) {
		g_application_bind_busy_property(
			gApplicationPointer,
			obj.pointer,
			property
		)
	}

	fun setActionGroup(actionGroup: ActionGroup) {
		g_application_set_action_group(
			gApplicationPointer,
			actionGroup.actionGroupPointer
		)
	}

	var applicationID: String?
		get() = g_application_get_application_id(gApplicationPointer)?.toKString()
		set(value) = g_application_set_application_id(
			gApplicationPointer,
			value
		)

	var inactivityTimeout: UInt
		get() = g_application_get_inactivity_timeout(gApplicationPointer)
		set(value) = g_application_set_inactivity_timeout(
			gApplicationPointer,
			value
		)

	var flags: GApplicationFlags
		get() = g_application_get_flags(gApplicationPointer)
		set(value) = g_application_set_flags(gApplicationPointer, value)

	var resourceBasePath: String?
		get() = g_application_get_resource_base_path(gApplicationPointer)?.toKString()
		set(value) = g_application_set_resource_base_path(
			gApplicationPointer,
			value
		)

	val dbusConnection: DBusConnection?
		get() = g_application_get_dbus_connection(gApplicationPointer).wrap()

	val dbusObjectPath: String?
		get() = g_application_get_dbus_object_path(gApplicationPointer)?.toKString()

	val isRegistered: Boolean
		get() = g_application_get_is_registered(gApplicationPointer).bool

	val isRemote: Boolean
		get() = g_application_get_is_remote(gApplicationPointer).bool

	val isBusy: Boolean
		get() = g_application_get_is_busy(gApplicationPointer).bool


	fun addOnActivateCallback(
		action: TypedNoArgFunc<Application>,
	): SignalManager =
		addSignalCallback(ACTIVATE, action, staticNoArgFunc)

	fun addOnCommandLineCallback(
		action: ApplicationCommandLineFunc,
	): SignalManager =
		addSignalCallback(COMMAND_LINE, action, staticCommandLineFunction)

	fun addOnHandleLocalOptionsCallback(
		action: ApplicationHandleLocalOptionsFunc,
	): SignalManager =
		addSignalCallback(HANDLE_LOCAL_OPTIONS, action, staticHandleLocalOptionsFunction)

	fun addOnNameLostCallback(
		action: TypedNoArgForBooleanFunc<Application>,
	): SignalManager =
		addSignalCallback(Signals.NAME_LOST, action, staticBoolFunc)

	fun addOnOpenCallback(
		action: ApplicationOpenFunc,
	): SignalManager =
		addSignalCallback(Signals.OPEN, action, staticOpenFunc)

	fun addOnShutdownCallback(action: TypedNoArgFunc<Application>): SignalManager =
		addSignalCallback(Signals.SHUTDOWN, action, staticNoArgFunc)

	fun addOnStartupCallback(action: TypedNoArgFunc<Application>): SignalManager =
		addSignalCallback(Signals.STARTUP, action, staticNoArgFunc)

	override val actionMapPointer: CPointer<GActionMap> =
		gApplicationPointer.reinterpret()

	constructor(applicationID: String?, flags: GApplicationFlags) : this(
		g_application_new(
			applicationID,
			flags
		)!!.reinterpret()
	)

	fun register(cancellable: KGCancellable): Boolean = memScoped {
		val err = allocPointerTo<GError>().ptr
		val r = g_application_register(
			gApplicationPointer,
			cancellable.cancellablePointer,
			err
		)
		err.unwrap()
		r.bool
	}

	fun hold() {
		g_application_hold(gApplicationPointer)
	}

	fun release() {
		g_application_release(gApplicationPointer)
	}

	fun quit() {
		g_application_quit(gApplicationPointer)
	}

	fun activate() {
		g_application_activate(gApplicationPointer)
	}

	fun open(files: Array<File>, hint: String = "") {
		g_application_open(
			gApplicationPointer,
			files.toCArray(),
			files.size,
			hint
		)
	}

	fun sendNotification(notification: Notification, id: String? = null) {
		g_application_send_notification(
			gApplicationPointer,
			id,
			notification.notificationPointer
		)
	}

	fun withdrawNotification(id: String) {
		g_application_withdraw_notification(gApplicationPointer, id)
	}

	fun run(argc: Int = 0, argv: Array<String> = arrayOf()): Int = memScoped {
		g_application_run(
			gApplicationPointer,
			argc,
			argv.toCStringArray(this)
		)
	}

	fun addMainOptionEntries(entries: Array<OptionEntry>) {
		memScoped {
			val alloc = entries.toNullTermCArray { struct }

			g_application_add_main_option_entries(
				gApplicationPointer,
				alloc.pointed.value
			)
		}
	}

	fun addMainOption(
		longName: String,
		shortName: Char,
		description: String,
		flags: OptionFlags = OptionFlags.NONE,
		arg: OptionArg = OptionArg.NONE,
		argDescription: String? = null,
	) {
		g_application_add_main_option(
			gApplicationPointer,
			longName,
			shortName.code.toByte(),
			flags.gtk,
			arg.gio,
			description,
			argDescription
		)
	}

	fun addOptionGroup(optionGroup: OptionGroup) {
		g_application_add_option_group(
			gApplicationPointer,
			optionGroup.optionPointer
		)
	}

	fun setOptionContextParameterString(parameterString: String?) {
		g_application_set_option_context_parameter_string(
			gApplicationPointer,
			parameterString
		)
	}

	fun setOptionContextSummary(summary: String?) {
		g_application_set_option_context_summary(
			gApplicationPointer,
			summary
		)
	}

	fun setOptionContextDescription(description: String?) {
		g_application_set_option_context_description(
			gApplicationPointer,
			description
		)
	}

	fun setDefault() {
		g_application_set_default(gApplicationPointer)
	}

	fun markBusy() =
		g_application_mark_busy(gApplicationPointer)

	fun markNotBusy() =
		g_application_unmark_busy(gApplicationPointer)

	fun unmarkBusy() {
		g_application_unmark_busy(gApplicationPointer)
	}

	data class OpenEvent(
		val files: Sequence<File>,
		val hint: String,
	)

	companion object {
		private val staticCommandLineFunction: GCallback =
			staticCFunction {
					self: GApplication_autoptr,
					commandLine: CPointer<GApplicationCommandLine>,
					data: gpointer,
				->
				data.asStableRef<ApplicationCommandLineFunc>()
					.get()
					.invoke(self.wrap(), commandLine.wrap())
			}.reinterpret()

		private val staticHandleLocalOptionsFunction: GCallback =
			staticCFunction {
					self: GApplication_autoptr,
					dict: CPointer<GVariantDict>,
					data: gpointer,
				->
				data.asStableRef<ApplicationHandleLocalOptionsFunc>()
					.get()
					.invoke(self.wrap(), dict.wrap())
			}.reinterpret()

		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: GApplication_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<Application>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticOpenFunc: GCallback =
			staticCFunction {
					self: GApplication_autoptr,
					files: CPointer<GFile_autoptrVar>,
					n_files: Int,
					hint: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<ApplicationOpenFunc>()
					.get()
					.invoke(
						self.wrap(),
						Array(n_files) { files[it].wrap()!! },
						hint.toKString()
					)
			}.reinterpret()


		private val staticBoolFunc: GCallback =
			staticCFunction {
					self: GApplication_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgForBooleanFunc<Application>>()
					.get()
					.invoke(self.wrap())
					.gtk
			}.reinterpret()

		val default: Application?
			get() = g_application_get_default().wrap()

		fun isIdValid(applicationID: String): Boolean =
			g_application_id_is_valid(applicationID).bool

		inline fun GApplication_autoptr?.wrap() =
			this?.let { Application(it) }

		inline fun GApplication_autoptr.wrap() =
			Application(this)
	}
}

typealias ApplicationCommandLineFunc =
		Application.(
			commandLine: ApplicationCommandLine,
		) -> Int

typealias ApplicationHandleLocalOptionsFunc =
		Application.(
			options: VariantDictionary,
		) -> Int

typealias ApplicationOpenFunc =
		Application.(
			array: Array<File>,
			hint: String,
		) -> Unit