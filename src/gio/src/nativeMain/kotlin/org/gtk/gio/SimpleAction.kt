package org.gtk.gio

import gio.*
import glib.GVariant_autoptr
import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.*
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.VariantType
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * kotlinx-gtk
 *
 * 23 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.SimpleAction.html">
 *     GSimpleAction</a>
 */
class SimpleAction(val simpleActionPointer: GSimpleAction_autoptr) :
	KGObject(simpleActionPointer.reinterpret()), Action {

	override val actionPointer: CPointer<GAction>
		get() = simpleActionPointer.reinterpret()

	/**
	 * @see <a href="https://docs.gtk.org/gio/ctor.SimpleAction.new.html">
	 *     g_simple_action_new</a>
	 */
	constructor(name: String, parameterType: VariantType? = null) : this(
		g_simple_action_new(name, parameterType?.variantTypePointer)!!
	)

	/**
	 * @see <a href="https://docs.gtk.org/gio/ctor.SimpleAction.new_stateful.html">
	 *     g_simple_action_new_stateful</a>
	 */
	constructor(
		name: String,
		state: Variant,
		parameterType: VariantType? = null,
	) : this(
		g_simple_action_new_stateful(
			name,
			parameterType?.variantTypePointer,
			state.variantPointer
		)!!
	)

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.SimpleAction.set_enabled.html">
	 *     g_simple_action_set_enabled</a>
	 */
	fun setEnabled(enabled: Boolean) {
		g_simple_action_set_enabled(simpleActionPointer, enabled.gtk)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.SimpleAction.set_state.html">
	 *     g_simple_action_set_state</a>
	 */
	fun setState(value: Variant) {
		g_simple_action_set_state(simpleActionPointer, value.variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.SimpleAction.set_state_hint.html">
	 *     g_simple_action_set_state_hint</a>
	 */
	fun setStateHint(stateHint: Variant) {
		g_simple_action_set_state_hint(
			simpleActionPointer,
			stateHint.variantPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/signal.SimpleAction.activate.html">
	 *     activate</a>
	 */
	fun addOnActivateCallback(action: (parameter: Variant?) -> Unit): SignalManager =
		addSignalCallback(Signals.ACTIVATE, action, staticActivateFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gio/signal.SimpleAction.change-state.html">
	 *     change-state</a>
	 */
	fun addOnChangeStateCallback(action: (value: Variant?) -> Unit): SignalManager =
		addSignalCallback(Signals.CHANGE_STATE, action, staticChangeStateFunction)

	companion object {
		private val staticActivateFunction: GCallback =
			staticCFunction { _: GSimpleAction_autoptr, parameter: GVariant_autoptr?, data: gpointer ->
				data.asStableRef<(Variant?) -> Unit>().get().invoke(parameter.wrap())
				Unit
			}.reinterpret()

		private val staticChangeStateFunction: GCallback =
			staticCFunction { _: GSimpleAction_autoptr, value: GVariant_autoptr?, data: gpointer ->
				data.asStableRef<(Variant?) -> Unit>().get().invoke(value.wrap())
				Unit
			}.reinterpret()

		/**
		 * Convert an [Action] safely as an
		 */
		fun Action.safeCast(): SimpleAction? =
			interpretCPointer<GSimpleAction>(actionPointer.rawValue)?.let {
				SimpleAction(it)
			}
	}
}