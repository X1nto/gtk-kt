package org.gtk.gio

import gio.*
import kotlinx.cinterop.reinterpret

/**
 * kotlinx-gtk
 *
 * 28 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.Menu.html">GMenu</a>
 */
open class Menu(
	val menuPointer: GMenu_autoptr
) : MenuModel(menuPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gio/ctor.Menu.new.html">
	 *     g_menu_new</a>
	 */
	constructor() : this(g_menu_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.append.html">
	 *     g_menu_append</a>
	 */
	fun append(label: String?, detailedAction: String?) {
		g_menu_append(menuPointer, label, detailedAction)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.append_item.html">
	 *     g_menu_append_item</a>
	 */
	fun appendItem(menuItem: MenuItem) {
		g_menu_append_item(menuPointer, menuItem.menuItemPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.append_section.html">
	 *     g_menu_append_section</a>
	 */
	fun appendSection(
		label: String?,
		section: MenuModel
	) {
		g_menu_append_section(menuPointer, label, section.menuModelPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.append_submenu.html">
	 *     g_menu_append_submenu</a>
	 */
	fun appendSubmenu(
		label: String?,
		section: MenuModel
	) {
		g_menu_append_submenu(menuPointer, label, section.menuModelPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.freeze.html">
	 *     g_menu_freeze</a>
	 */
	fun freeze() {
		g_menu_freeze(menuPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.insert.html">
	 *     g_menu_insert</a>
	 */
	fun insert(position: Int, label: String?, detailedAction: String?) {
		g_menu_insert(menuPointer, position, label, detailedAction)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.insert_item.html">
	 *     g_menu_insert_item</a>
	 */
	fun insertItem(position: Int, menuItem: MenuItem) {
		g_menu_insert_item(menuPointer, position, menuItem.menuItemPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.insert_section.html">
	 *     g_menu_insert_section</a>
	 */
	fun insertSection(
		position: Int,
		label: String?,
		section: MenuModel
	) {
		g_menu_insert_section(
			menuPointer,
			position,
			label,
			section.menuModelPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.insert_submenu.html">
	 *     g_menu_insert_submenu</a>
	 */
	fun insertSubmenu(
		position: Int,
		label: String?,
		submenu: MenuModel
	) {
		g_menu_insert_submenu(
			menuPointer,
			position,
			label,
			submenu.menuModelPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.prepend.html">
	 *     g_menu_prepend</a>
	 */
	fun prepend(label: String?, detailedAction: String?) {
		g_menu_prepend(menuPointer, label, detailedAction)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.prepend_item.html">
	 *     g_menu_prepend_item</a>
	 */
	fun prependItem(menuItem: MenuItem) {
		g_menu_prepend_item(menuPointer, menuItem.menuItemPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.prepend_section.html">
	 *     g_menu_prepend_section</a>
	 */
	fun prependSection(
		label: String?,
		section: MenuModel
	) {
		g_menu_prepend_section(menuPointer, label, section.menuModelPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.prepend_submenu.html">
	 *     g_menu_prepend_submenu</a>
	 */
	fun prependSubmenu(
		label: String?,
		submenu: MenuModel
	) {
		g_menu_prepend_submenu(menuPointer, label, submenu.menuModelPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.remove.html">
	 *     g_menu_remove</a>
	 */
	fun remove(position: Int) {
		g_menu_remove(menuPointer, position)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Menu.remove_all.html">
	 *     g_menu_remove_all</a>
	 */
	fun removeAll() {
		g_menu_remove_all(menuPointer)
	}

	companion object {
		inline fun GMenu_autoptr?.wrap() =
			this?.wrap()

		inline fun GMenu_autoptr.wrap() =
			Menu(this)
	}
}