package org.gtk.gio

import gio.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import org.gtk.gio.Icon.Companion.wrap

class FileInfo(val fileInfoPointer: CPointer<GFileInfo>) {

    var name: String?
        get() = g_file_info_get_name(fileInfoPointer)?.toKString()
        set(value) = g_file_info_set_name(fileInfoPointer, value)

    var icon: Icon?
        get() = g_file_info_get_icon(fileInfoPointer).wrap()
        set(value) = g_file_info_set_icon(fileInfoPointer, value?.iconPointer)

    companion object {

        inline fun GFileInfo_autoptr?.wrap() =
            this?.wrap()

        fun GFileInfo_autoptr.wrap(): FileInfo =
            FileInfo(this)
    }
}