package org.gtk.gio

import gio.GFileOutputStream_autoptr
import gio.g_file_output_stream_get_etag
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gio/class.FileOutputStream.html">
 *     GFileOutputStream</a>
 */
class FileOutputStream(
	val fileOutputPointer: GFileOutputStream_autoptr,
) : OutputStream(fileOutputPointer.reinterpret()) {
	val etag: String?
		get() = g_file_output_stream_get_etag(fileOutputPointer)?.toKString()

	companion object {

		inline fun GFileOutputStream_autoptr?.wrap() =
			this?.wrap()

		inline fun GFileOutputStream_autoptr.wrap() =
			FileOutputStream(this)
	}
}