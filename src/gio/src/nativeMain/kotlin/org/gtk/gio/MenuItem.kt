package org.gtk.gio

import gio.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.VariantType
import org.gtk.glib.bool
import org.gtk.gobject.KGObject

/**
 * kotlinx-gtk
 *
 * 28 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.MenuItem.html">GMenuItem</a>
 */
open class MenuItem(
	val menuItemPointer: GMenuItem_autoptr
) : KGObject(menuItemPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gio/ctor.MenuItem.new.html">
	 *     g_menu_item_new</a>
	 */
	constructor(label: String? = null, detailedAction: String? = null) : this(
		g_menu_item_new(label, detailedAction)!!.reinterpret()
	)

	/**
	 * @see <a href="https://docs.gtk.org/gio/ctor.MenuItem.new_from_model.html">
	 *     g_menu_item_new_from_model</a>
	 */
	constructor(model: MenuModel, index: Int) : this(
		g_menu_item_new_from_model(
			model.menuModelPointer,
			index
		)!!.reinterpret()
	)

	class Section(p: GMenuItem_autoptr) : MenuItem(p) {
		/**
		 * @see <a href="https://docs.gtk.org/gio/ctor.MenuItem.new_section.html">
		 *     g_menu_item_new_section</a>
		 */
		constructor(label: String?, section: MenuModel) : this(
			g_menu_item_new_section(
				label,
				section.menuModelPointer
			)!!.reinterpret()
		)
	}

	class Submenu(p: GMenuItem_autoptr) : MenuItem(p) {
		/**
		 * @see <a href="https://docs.gtk.org/gio/ctor.MenuItem.new_submenu.html">
		 *     g_menu_item_new_submenu</a>
		 */
		constructor(label: String?, section: MenuModel) : this(
			g_menu_item_new_submenu(
				label,
				section.menuModelPointer
			)!!.reinterpret()
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.get_attribute.html">
	 *     g_menu_item_get_attribute</a>
	 */
	fun getAttribute(attribute: String, formatString: String): Boolean =
		g_menu_item_get_attribute(menuItemPointer, attribute, formatString).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.get_attribute_value.html">
	 *     g_menu_item_get_attribute_value</a>
	 */
	fun getAttributeValue(
		attribute: String,
		expectedType: VariantType? = null
	): Variant? =
		g_menu_item_get_attribute_value(
			menuItemPointer,
			attribute,
			expectedType?.variantTypePointer
		).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.get_link.html">
	 *     g_menu_item_get_link</a>
	 */
	fun getLink(link: String): MenuModel? =
		g_menu_item_get_link(menuItemPointer, link).wrap()


	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_action_and_target.html">
	 *     g_menu_item_set_action_and_target</a>
	 */
	fun setActionAndTargetValue(action: String?, formatString: String?) {
		g_menu_item_set_action_and_target(menuItemPointer, action, formatString)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_action_and_target_value.html">
	 *     g_menu_item_set_action_and_target_value</a>
	 */
	fun setActionAndTargetValue(action: String?, variant: Variant?) {
		g_menu_item_set_action_and_target_value(
			menuItemPointer,
			action,
			variant?.variantPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_attribute.html">
	 *     g_menu_item_set_attribute</a>
	 */
	fun setAttribute(attribute: String, formatString: String?) {
		g_menu_item_set_attribute(menuItemPointer, attribute, formatString)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_attribute_value.html">
	 *     g_menu_item_set_attribute_value</a>
	 */
	fun setAttributeValue(attribute: String, variant: Variant?) {
		g_menu_item_set_attribute_value(
			menuItemPointer,
			attribute,
			variant?.variantPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_detailed_action.html">
	 *     g_menu_item_set_detailed_action</a>
	 */
	fun setDetailedAction(detailedAction: String) {
		g_menu_item_set_detailed_action(menuItemPointer, detailedAction)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_icon.html">
	 *     g_menu_item_set_icon</a>
	 */
	fun setIcon(icon: Icon?) {
		g_menu_item_set_icon(menuItemPointer, icon?.iconPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_label.html">
	 *     g_menu_item_set_label</a>
	 */
	fun setLabel(label: String?) {
		g_menu_item_set_label(menuItemPointer, label)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_link.html">
	 *     g_menu_item_set_link</a>
	 */
	fun setLink(link: String, menuModel: MenuModel?) {
		g_menu_item_set_link(
			menuItemPointer,
			link,
			menuModel?.menuModelPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_section.html">
	 *     g_menu_item_set_section</a>
	 */
	fun setSection(section: MenuModel?) {
		g_menu_item_set_section(menuItemPointer, section?.menuModelPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuItem.set_submenu.html">
	 *     g_menu_item_set_submenu</a>
	 */
	fun setSubmenu(submenu: MenuModel?) {
		g_menu_item_set_submenu(menuItemPointer, submenu?.menuModelPointer)
	}

}
