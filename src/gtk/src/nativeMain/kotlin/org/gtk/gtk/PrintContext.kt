package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.*
import org.gtk.cairo.Cairo
import org.gtk.cairo.Cairo.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.PageSetup.Companion.wrap
import org.gtk.pango.Context
import org.gtk.pango.Context.Companion.wrap
import org.gtk.pango.FontMap
import org.gtk.pango.FontMap.Companion.wrap
import org.gtk.pango.Layout
import org.gtk.pango.Layout.Companion.wrap

/**
 * 06 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.PrintContext.html">
 *     GtkPrintContext</a>
 */
class PrintContext(
	val printContextPointer: GtkPrintContext_autoptr,
) : KGObject(printContextPointer.reinterpret()) {

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_PRINT_CONTEXT))

	fun createPangoContext(): Context =
		gtk_print_context_create_pango_context(
			printContextPointer
		)!!.wrap()

	fun createPangoLayout(): Layout =
		gtk_print_context_create_pango_layout(
			printContextPointer
		)!!.wrap()

	val cairoContext: Cairo
		get() = gtk_print_context_get_cairo_context(
			printContextPointer
		)!!.wrap()

	val dpiX: Double
		get() = gtk_print_context_get_dpi_x(printContextPointer)

	val dpiY: Double
		get() = gtk_print_context_get_dpi_y(printContextPointer)


	data class HardMargins(
		val top: Double,
		val bottom: Double,
		val left: Double,
		val right: Double,
	)

	val hardMargins: HardMargins?
		get() = memScoped {
			val top = cValue<DoubleVar>()
			val bottom = cValue<DoubleVar>()
			val left = cValue<DoubleVar>()
			val right = cValue<DoubleVar>()

			val result = gtk_print_context_get_hard_margins(
				printContextPointer,
				top,
				bottom,
				left,
				right
			)

			if (result.bool)
				HardMargins(
					top.ptr.pointed.value,
					bottom.ptr.pointed.value,
					left.ptr.pointed.value,
					right.ptr.pointed.value
				)
			else null
		}

	val height: Double
		get() = gtk_print_context_get_height(printContextPointer)

	val pageSetup: PageSetup
		get() = gtk_print_context_get_page_setup(printContextPointer)!!.wrap()

	val pangoFontMap: FontMap
		get() = gtk_print_context_get_pango_fontmap(printContextPointer)!!.wrap()

	val width: Double
		get() = gtk_print_context_get_width(printContextPointer)

	fun setCairoContext(cairo: Cairo, dpiX: Double, dpiY: Double) {
		gtk_print_context_set_cairo_context(
			printContextPointer,
			cairo.pointer,
			dpiX,
			dpiY
		)
	}
}