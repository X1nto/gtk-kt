package org.gtk.gtk

import gio.GListModel_autoptr
import glib.gpointer
import gobject.GObject
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.asStablePointer
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.staticDestroyStableRefFunction

/**
 * 04 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MapListModel.html">
 *     GtkMapListModel</a>
 */
class MapListModel(
	val mapListModelPointer: GtkMapListModel_autoptr,
) : KGObject(mapListModelPointer.reinterpret()), ListModel {
	override val listModelPointer: GListModel_autoptr by lazy {
		mapListModelPointer.reinterpret()
	}

	constructor(
		model: ListModel?,
		mapFunc: MapListModelMapFunc?,
	) : this(
		gtk_map_list_model_new(
			model?.listModelPointer,
			if (mapFunc != null) staticMapFunc else null,
			mapFunc?.asStablePointer(),
			if (mapFunc != null) staticDestroyStableRefFunction else null
		)!!
	)

	var model: ListModel?
		get() = gtk_map_list_model_get_model(mapListModelPointer).wrap()
		set(value) = gtk_map_list_model_set_model(
			mapListModelPointer,
			value?.listModelPointer
		)

	val hasMap: Boolean
		get() = gtk_map_list_model_has_map(mapListModelPointer).bool

	fun setMapFunc(mapFunc: MapListModelMapFunc?) {
		gtk_map_list_model_set_map_func(
			mapListModelPointer,
			if (mapFunc != null) staticMapFunc else null,
			mapFunc?.asStablePointer(),
			if (mapFunc != null) staticDestroyStableRefFunction else null
		)
	}

	companion object {

		inline fun GtkMapListModel_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkMapListModel_autoptr.wrap() =
			MapListModel(this)

		private val staticMapFunc: GtkMapListModelMapFunc =
			staticCFunction {
					item: gpointer?,
					data: gpointer?,
				->
				data!!.asStableRef<MapListModelMapFunc>()
					.get()
					.invoke(
						item!!.reinterpret<GObject>().wrap()
					).pointer
			}
	}
}

/**
 * @see <a href="https://docs.gtk.org/gtk4/callback.MapListModelMapFunc.html">
 *     GtkMapListModelMapFunc</a>
 */
typealias MapListModelMapFunc =
			(
			item: KGObject,
		) -> KGObject