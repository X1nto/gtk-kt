package org.gtk.gtk.action

import gtk.GTK_TYPE_NOTHING_ACTION
import gtk.GtkNothingAction_autoptr
import gtk.gtk_nothing_action_get
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.NothingAction.html">
 *     GtkNothingAction</a>
 */
class NothingAction(val mnemonicAction: GtkNothingAction_autoptr) :
	ShortcutAction(mnemonicAction.reinterpret()) {

	constructor() : this(gtk_nothing_action_get()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_NOTHING_ACTION))

	companion object {
		inline fun GtkNothingAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkNothingAction_autoptr.wrap() =
			NothingAction(this)
	}
}