package org.gtk.gtk.controller.gesture

import gtk.GTK_TYPE_GESTURE_ROTATE
import gtk.GtkGestureRotate_autoptr
import gtk.gtk_gesture_rotate_get_angle_delta
import gtk.gtk_gesture_rotate_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GestureRotate.html">
 *     GtkGestureRotate</a>
 */
class GestureRotate(
	val rotatePointer: GtkGestureRotate_autoptr,
) : Gesture(rotatePointer.reinterpret()) {

	constructor() : this(gtk_gesture_rotate_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_ROTATE))

	val angleDelta: Double
		get() = gtk_gesture_rotate_get_angle_delta(rotatePointer)
}