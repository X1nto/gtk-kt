package org.gtk.gtk.cellrenderer

import gtk.GTK_TYPE_CELL_RENDERER_PROGRESS
import gtk.GtkCellRendererProgress_autoptr
import gtk.gtk_cell_renderer_progress_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRendererProgress.html">
 *     GtkCellRendererProgress</a>
 */
class CellRendererProgress(
	val progressPointer: GtkCellRendererProgress_autoptr,
) : CellRenderer(progressPointer.reinterpret()) {

	constructor() : this(gtk_cell_renderer_progress_new()!!.reinterpret())

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_CELL_RENDERER_PROGRESS
	))
}