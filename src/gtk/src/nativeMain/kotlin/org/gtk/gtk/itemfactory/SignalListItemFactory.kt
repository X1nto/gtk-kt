package org.gtk.gtk.itemfactory

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_SIGNAL_LIST_ITEM_FACTORY
import gtk.GtkListItem
import gtk.GtkSignalListItemFactory
import gtk.gtk_signal_list_item_factory_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.SignalListItemFactoryCallbackFunction
import org.gtk.gtk.itemfactory.ListItem.Companion.wrap

/**
 * 27 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SignalListItemFactory.html">
 *     GtkSignalListItemFactory</a>
 */
class SignalListItemFactory(
	val signalListItemFactoryPointer: CPointer<GtkSignalListItemFactory>
) : ListItemFactory(signalListItemFactoryPointer.reinterpret()) {

	constructor() : this(gtk_signal_list_item_factory_new()!!.reinterpret())

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_SIGNAL_LIST_ITEM_FACTORY
	))

	fun addOnSetupCallback(action: SignalListItemFactoryCallbackFunction) =
		addSignalCallback(Signals.SETUP, action, staticSignalFunction)

	fun addOnBindCallback(action: SignalListItemFactoryCallbackFunction) =
		addSignalCallback(Signals.BIND, action, staticSignalFunction)

	fun addOnUnbindCallback(action: SignalListItemFactoryCallbackFunction) =
		addSignalCallback(Signals.UNBIND, action, staticSignalFunction)

	fun addOnTeardownCallback(action: SignalListItemFactoryCallbackFunction) =
		addSignalCallback(Signals.TEARDOWN, action, staticSignalFunction)

	companion object {

		private val staticSignalFunction: GCallback =
			staticCFunction { factory: CPointer<GtkSignalListItemFactory>,
			                  listItem: CPointer<GtkListItem>,
			                  data: gpointer ->
				data.asStableRef<SignalListItemFactoryCallbackFunction>()
					.get()
					.invoke(factory.wrap(), listItem.wrap())
			}.reinterpret()

		fun CPointer<GtkSignalListItemFactory>.wrap() =
			SignalListItemFactory(this)

		fun CPointer<GtkSignalListItemFactory>?.wrap() =
			this?.wrap()
	}
}