package org.gtk.gtk

import gtk.GtkActionable_autoptr

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/iface.Actionable.html">
 *     GtkActionable</a>
 */
interface Actionable {
	val actionablePointer: GtkActionable_autoptr
}