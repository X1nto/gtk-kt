package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.Variant
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget

/**
 * 26 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutAction.html">
 *     GtkShortcutAction</a>
 */
class ShortcutAction(
	val actionPointer: GtkShortcutAction_autoptr,
) : KGObject(actionPointer.reinterpret()) {

	fun activate(
		flags: GtkShortcutActionFlags,
		widget: Widget,
		args: Variant,
	): Boolean =
		gtk_shortcut_action_activate(
			actionPointer,
			flags,
			widget.widgetPointer,
			args.variantPointer
		).bool

	// ignore gtk_shortcut_action_print as its for debug

	override fun toString(): String =
		gtk_shortcut_action_to_string(actionPointer)!!.toKString()

	companion object {
		inline fun GtkShortcutAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkShortcutAction_autoptr.wrap() =
			ShortcutAction(this)

		fun parseString(string: String): ShortcutAction? =
			gtk_shortcut_action_parse_string(string).wrap()
	}
}