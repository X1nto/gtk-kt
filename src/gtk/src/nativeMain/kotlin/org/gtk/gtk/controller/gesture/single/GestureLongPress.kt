package org.gtk.gtk.controller.gesture.single

import glib.gdouble
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gobject.TypedNoArgFunc

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GestureLongPress.html">
 *     GtkGestureLongPress</a>
 */
class GestureLongPress(
	val longPressPointer: GtkGestureLongPress_autoptr,
) : GestureSingle(longPressPointer.reinterpret()) {

	constructor() : this(gtk_gesture_long_press_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_LONG_PRESS))

	var delayFactor: Double
		get() = gtk_gesture_long_press_get_delay_factor(longPressPointer)
		set(value) = gtk_gesture_long_press_set_delay_factor(
			longPressPointer,
			value
		)

	fun addOnCancelledCallback(action: TypedNoArgFunc<GestureLongPress>) =
		addSignalCallback(Signals.CANCELLED, action, staticNoArgGCallback)

	fun addOnPressedCallback(action: GestureLongPressPressedFunc) =
		addSignalCallback(Signals.SWIPE, action, staticPressedFunc)


	companion object {

		inline fun GtkGestureLongPress_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGestureLongPress_autoptr.wrap() =
			GestureLongPress(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction {
					self: GtkGestureLongPress_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<GestureLongPress>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticPressedFunc: GCallback =
			staticCFunction {
					self: GtkGestureLongPress_autoptr,
					x: gdouble,
					y: gdouble,
					data: gpointer,
				->
				data.asStableRef<GestureLongPressPressedFunc>()
					.get()
					.invoke(self.wrap(), x, y)
			}.reinterpret()
	}
}

typealias GestureLongPressPressedFunc =
		GestureLongPress.(
			x: Double,
			y: Double,
		) -> Unit