package org.gtk.gtk.widgets

import gtk.GTK_TYPE_SHORTCUTS_SHORTCUT
import gtk.GtkShortcutsShortcut
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutsShortcut.html">
 *     GtkShortcutsShortcut</a>
 */
class ShortcutsShortcut(
	val shortcutsShortcutPointer: CPointer<GtkShortcutsShortcut>,
) : Widget(shortcutsShortcutPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_SHORTCUTS_SHORTCUT))
}