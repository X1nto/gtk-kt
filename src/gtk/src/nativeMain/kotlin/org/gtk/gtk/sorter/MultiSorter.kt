package org.gtk.gtk.sorter

import gio.GListModel_autoptr
import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gtk.Buildable

/**
 * 17 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MultiSorter.html">
 *     GtkMultiSorter</a>
 */
class MultiSorter(val multiSorterPointer: GtkMultiSorter_autoptr) :
	Sorter(multiSorterPointer.reinterpret()), ListModel, Buildable {

	constructor() : this(gtk_multi_sorter_new()!!)

	fun append(sorter: Sorter) {
		gtk_multi_sorter_append(multiSorterPointer, sorter.sorterPointer)
	}

	fun remove(position: UInt) {
		gtk_multi_sorter_remove(multiSorterPointer, position)
	}

	override val listModelPointer: GListModel_autoptr by lazy {
		multiSorterPointer.reinterpret()
	}

	override val buildablePointer: GtkBuildable_autoptr by lazy {
		multiSorterPointer.reinterpret()
	}
}