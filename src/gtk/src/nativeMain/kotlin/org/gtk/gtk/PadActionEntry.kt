package org.gtk.gtk

import gtk.GtkPadActionEntry
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.nativeHeap
import kotlinx.cinterop.ptr

/**
 * 14 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/struct.PadActionEntry.html">
 *     GtkPadActionEntry</a>
 */
class PadActionEntry(
	val struct: CPointer<GtkPadActionEntry>,
) {
	constructor() : this(nativeHeap.alloc<GtkPadActionEntry>().ptr)
}