package org.gtk.gtk.manager

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.Orientable
import org.gtk.gtk.common.enums.BaselinePosition
import org.gtk.gtk.common.enums.Orientation

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.BoxLayout.html">
 *     GtkBoxLayout</a>
 */
class BoxLayout(val boxLayoutPointer: GtkBoxLayout_autoptr) :
	LayoutManager(boxLayoutPointer.reinterpret()), Orientable {

	override val orientablePointer: CPointer<GtkOrientable> by lazy {
		boxLayoutPointer.reinterpret()
	}

	constructor(orientation: Orientation) :
			this(gtk_box_layout_new(orientation.gtk)!!.reinterpret())

	var baseLinePosition: BaselinePosition
		get() = BaselinePosition.valueOf(
			gtk_box_layout_get_baseline_position(boxLayoutPointer)
		)
		set(value) = gtk_box_layout_set_baseline_position(
			boxLayoutPointer,
			value.gtk
		)

	var isHomogeneous: Boolean
		get() = gtk_box_layout_get_homogeneous(boxLayoutPointer).bool
		set(value) = gtk_box_layout_set_homogeneous(boxLayoutPointer, value.gtk)

	var spacing: UInt
		get() = gtk_box_layout_get_spacing(boxLayoutPointer)
		set(value) = gtk_box_layout_set_spacing(boxLayoutPointer, value)

}