package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutLabel.html">
 *     GtkShortcutLabel</a>
 */
class ShortcutLabel(
	val shortcutLabelPointer: CPointer<GtkShortcutLabel>,
) : Widget(shortcutLabelPointer.reinterpret()) {

	constructor(accelerator: String) :
			this(gtk_shortcut_label_new(accelerator)!!.reinterpret())

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_SHORTCUT_LABEL))

	var accelerator: String?
		get() = gtk_shortcut_label_get_accelerator(
			shortcutLabelPointer
		)?.toKString()
		set(value) = gtk_shortcut_label_set_accelerator(
			shortcutLabelPointer,
			value
		)

	var disabledText: String?
		get() = gtk_shortcut_label_get_disabled_text(
			shortcutLabelPointer
		)?.toKString()
		set(value) = gtk_shortcut_label_set_disabled_text(
			shortcutLabelPointer,
			value
		)

}