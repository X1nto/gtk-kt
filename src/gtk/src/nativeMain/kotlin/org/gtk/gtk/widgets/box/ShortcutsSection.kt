package org.gtk.gtk.widgets.box

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_SHORTCUTS_SECTION
import gtk.GtkShortcutsSection
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.gtk
import org.gtk.gobject.Signals.CHANGE_CURRENT_PAGE
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.common.callback.TypedChangeCurrentPageFunction
import org.gtk.gtk.widgets.Widget

/**
 * gtk-kt
 *
 * 18 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutsSection.html">
 *     GtkShortcutsSection</a>
 */
class ShortcutsSection(
	val shortcutsSectionPointer: CPointer<GtkShortcutsSection>
) : Box(shortcutsSectionPointer.reinterpret()) {

	constructor(widget: Widget) : this(
		typeCheckInstanceCastOrThrow(
			widget,
			GTK_TYPE_SHORTCUTS_SECTION
		)
	)

	fun addOnChangeCurrentPageFunction(
		action: TypedChangeCurrentPageFunction<ShortcutsSection>
	) = addSignalCallback(
		CHANGE_CURRENT_PAGE,
		action,
		staticChangeCurrentPageFunction
	)

	companion object {
		private val staticChangeCurrentPageFunction: GCallback =
			staticCFunction { self: CPointer<GtkShortcutsSection>, arg1: Int, data: gpointer? ->
				data?.asStableRef<TypedChangeCurrentPageFunction<ShortcutsSection>>()?.get()
					?.invoke(self.wrap(), arg1).gtk
			}.reinterpret()

		inline fun CPointer<GtkShortcutsSection>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkShortcutsSection>.wrap() =
			ShortcutsSection(this)
	}
}
