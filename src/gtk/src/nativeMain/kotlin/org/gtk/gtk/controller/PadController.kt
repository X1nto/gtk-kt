package org.gtk.gtk.controller

import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Device
import org.gtk.gio.ActionGroup
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.PadActionEntry
import org.gtk.gtk.common.enums.PadActionType

/**
 * 14 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.PadController.html">
 *     GtkPadController</a>
 */
class PadController(
	val padPointer: CPointer<GtkPadController>,
) : EventController(padPointer.reinterpret()) {

	constructor(
		group: ActionGroup,
		pad: Device,
	) : this(
		gtk_pad_controller_new(
			group.actionGroupPointer,
			pad.pointer
		)!!.reinterpret()
	)

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_PAD_CONTROLLER
	))

	fun setAction(
		type: PadActionType,
		index: Int,
		mode: Int,
		label: String,
		actionName: String,
	) {
		gtk_pad_controller_set_action(
			padPointer,
			type.gtk,
			index,
			mode,
			label,
			actionName
		)
	}

	fun setActionEntries(entries: Array<PadActionEntry>) {
		memScoped {
			gtk_pad_controller_set_action_entries(
				padPointer,
				allocArrayOfPointersTo(
					entries.map { it.struct.pointed }
				).pointed.value,
				entries.size
			)
		}
	}
}