package org.gtk.gtk

import gtk.GtkStringObject
import gtk.gtk_string_object_get_string
import gtk.gtk_string_object_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 09 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.StringObject.html">
 *     GtkStringObject</a>
 */
class StringObject(val stringObjectPointer: CPointer<GtkStringObject>) : KGObject(stringObjectPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.StringObject.new.html">
	 *     gtk_string_object_new</a>
	 */
	constructor(string: String) : this(gtk_string_object_new(string)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StringObject.get_string.html">
	 *     gtk_string_object_get_string</a>
	 */
	val string: String
		get() = gtk_string_object_get_string(stringObjectPointer)!!.toKString()
}