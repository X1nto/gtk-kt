package org.gtk.gtk.trigger

import gtk.GTK_TYPE_MNEMONIC_TRIGGER
import gtk.GtkMnemonicTrigger_autoptr
import gtk.gtk_mnemonic_trigger_get_keyval
import gtk.gtk_mnemonic_trigger_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MnemonicTrigger.html">
 *     GtkMnemonicTrigger</a>
 */
class MnemonicTrigger(
	val mnemonicTriggerPointer: GtkMnemonicTrigger_autoptr,
) : ShortcutTrigger(mnemonicTriggerPointer.reinterpret()) {

	constructor(keyval: UInt) :
			this(gtk_mnemonic_trigger_new(keyval)!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_MNEMONIC_TRIGGER))

	val keyval: UInt
		get() = gtk_mnemonic_trigger_get_keyval(mnemonicTriggerPointer)
}