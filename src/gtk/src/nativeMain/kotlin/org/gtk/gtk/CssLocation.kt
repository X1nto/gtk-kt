package org.gtk.gtk

import gtk.GtkCssLocation
import kotlinx.cinterop.*

/**
 * 04 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/struct.CssLocation.html">
 *     GtkCssLocation</a>
 */
class CssLocation(
	val struct: CPointer<GtkCssLocation>,
) {
	constructor() : this(nativeHeap.alloc<GtkCssLocation>().ptr)

	var bytes: ULong
		get() = struct.pointed.bytes
		set(value) {
			struct.pointed.bytes = value
		}

	var chars: ULong
		get() = struct.pointed.chars
		set(value) {
			struct.pointed.chars = value
		}

	var lines: ULong
		get() = struct.pointed.lines
		set(value) {
			struct.pointed.lines = value
		}

	var lineBytes: ULong
		get() = struct.pointed.line_bytes
		set(value) {
			struct.pointed.line_bytes = value
		}

	var lineChars: ULong
		get() = struct.pointed.line_chars
		set(value) {
			struct.pointed.line_chars = value
		}

	companion object {

		inline fun CPointer<GtkCssLocation>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkCssLocation>.wrap() =
			CssLocation(this)
	}
}