package org.gtk.gtk

import gio.GListModel_autoptr
import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.gobject.KGObject
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 16 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FlattenListModel.html">
 *     GtkFlattenListModel</a>
 */
class FlattenListModel(
	val filterPointer: GtkFlattenListModel_autoptr,
) : KGObject(filterPointer.reinterpret()), ListModel {

	override val listModelPointer: GListModel_autoptr by lazy {
		filterPointer.reinterpret()
	}

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_FLATTEN_LIST_MODEL
	))

	constructor(model: ListModel?) :
			this(gtk_flatten_list_model_new(model?.listModelPointer)!!)

	var model: ListModel?
		get() = gtk_flatten_list_model_get_model(filterPointer).wrap()
		set(value) = gtk_flatten_list_model_set_model(
			filterPointer,
			value?.listModelPointer
		)

	fun getModelForItem(position: UInt): ListModel? =
		gtk_flatten_list_model_get_model_for_item(
			filterPointer,
			position
		).wrap()
}