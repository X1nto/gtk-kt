package org.gtk.gtk.widgets

import glib.gint
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.glib.CStringPointer
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.FileChooser

/**
 * 19 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FileChooserWidget.html">
 *     GtkFileChooserWidget</a>
 */
class FileChooserWidget(
	val fileChooserWidgetPointer: GtkFileChooserWidget_autoptr,
) : Widget(fileChooserWidgetPointer.reinterpret()), FileChooser {

	override val fileChooserPointer: CPointer<GtkFileChooser>
			by lazy { fileChooserWidgetPointer.reinterpret() }

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_FILE_CHOOSER_WIDGET))

	constructor(action: FileChooser.Action) :
			this(gtk_file_chooser_widget_new(action.gtk)!!.reinterpret())

	fun addOnDesktopFolderCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.DESKTOP_FOLDER, action, staticNoArgFunction)

	fun addOnDownFolderCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.DOWN_FOLDER, action, staticNoArgFunction)

	fun addOnHomeFolderCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.HOME_FOLDER, action, staticNoArgFunction)

	fun addOnLocationPopupCallback(action: FileChooserWidgetLocationPopupFunc) =
		addSignalCallback(Signals.LOCATION_POPUP, action, staticLocationPopupFunction)

	fun addOnLocationPopupOnPasteCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.LOCATION_POPUP_ON_PASTE, action, staticNoArgFunction)

	fun addOnLocationTogglePopupCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.LOCATION_TOGGLE_POPUP, action, staticNoArgFunction)

	fun addOnPlacesShortcutCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.PLACES_SHORTCUT, action, staticNoArgFunction)

	fun addOnQuickBookmarkCallback(action: FileChooserWidgetQuickBookmark) =
		addSignalCallback(Signals.QUICK_BOOKMARK, action, staticQuickBookmarkFunction)

	fun addOnRecentShortcutCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.RECENT_SHORTCUT, action, staticNoArgFunction)

	fun addOnSearchShortcutCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.SEARCH_SHORTCUT, action, staticNoArgFunction)

	fun addOnShowHiddenCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.SHOW_HIDDEN, action, staticNoArgFunction)

	fun addOnUpFolderCallback(action: FileChooserWidgetNoArgFunc) =
		addSignalCallback(Signals.UP_FOLDER, action, staticNoArgFunction)

	companion object {
		private val staticNoArgFunction: GCallback =
			staticCFunction {
					self: GtkFileChooserWidget_autoptr,
					data: gpointer,
				->
				data.asStableRef<FileChooserWidgetNoArgFunc>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticLocationPopupFunction: GCallback =
			staticCFunction {
					self: GtkFileChooserWidget_autoptr,
					path: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<FileChooserWidgetLocationPopupFunc>()
					.get()
					.invoke(self.wrap(), path.toKString())
			}.reinterpret()

		private val staticQuickBookmarkFunction: GCallback =
			staticCFunction {
					self: GtkFileChooserWidget_autoptr,
					bookmark_index: gint,
					data: gpointer,
				->
				data.asStableRef<FileChooserWidgetQuickBookmark>()
					.get()
					.invoke(self.wrap(), bookmark_index)
			}.reinterpret()

		inline fun GtkFileChooserWidget_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkFileChooserWidget_autoptr.wrap() =
			FileChooserWidget(this)
	}
}

typealias FileChooserWidgetNoArgFunc = FileChooserWidget.() -> Unit

typealias FileChooserWidgetLocationPopupFunc =
		FileChooserWidget.(
			path: String,
		) -> Unit

typealias FileChooserWidgetQuickBookmark =
		FileChooserWidget.(
			bookmarkIndex: Int,
		) -> Unit