package org.gtk.gtk.controller

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_EVENT_CONTROLLER_LEGACY
import gtk.GdkEvent_autoptr
import gtk.GtkEventControllerLegacy
import gtk.gtk_event_controller_legacy_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.Event
import org.gtk.gdk.Event.Companion.wrap
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 14 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EventControllerLegacy.html">
 *     GtkEventControllerLegacy</a>
 */
class EventControllerLegacy(
	private val legacyPointer: CPointer<GtkEventControllerLegacy>,
) : EventController(legacyPointer.reinterpret()) {

	constructor() : this(gtk_event_controller_legacy_new()!!.reinterpret())

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_EVENT_CONTROLLER_LEGACY
	))

	fun addOnEventCallback(action: EventControllerLegacyEventFunc) =
		addSignalCallback(
			Signals.EVENT,
			action,
			staticEventFunc
		)

	companion object {
		inline fun CPointer<GtkEventControllerLegacy>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkEventControllerLegacy>.wrap() =
			EventControllerLegacy(this)

		private val staticEventFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerLegacy>,
					event: GdkEvent_autoptr,
					data: gpointer,
				->
				data.asStableRef<EventControllerLegacyEventFunc>()
					.get()
					.invoke(self.wrap(), event.wrap())
					.gtk
			}.reinterpret()
	}

}

typealias EventControllerLegacyEventFunc =
		EventControllerLegacy.(
			event: Event,
		) -> Boolean