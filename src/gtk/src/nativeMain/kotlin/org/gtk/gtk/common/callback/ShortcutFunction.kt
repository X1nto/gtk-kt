package org.gtk.gtk.common.callback

import glib.GVariant_autoptr
import glib.gpointer
import gtk.GtkShortcutFunc
import gtk.GtkWidget_autoptr
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.gtk
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

typealias ShortcutFunction = (widget: Widget, args: Variant?) -> Boolean

internal val staticShortcutFunction: GtkShortcutFunc =
	staticCFunction { widget: GtkWidget_autoptr?, args: GVariant_autoptr?, data: gpointer? ->
		data!!.asStableRef<ShortcutFunction>()
			.get()
			.invoke(widget!!.wrap(), args.wrap())
			.gtk
	}