package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gtk.itemfactory.ListItemFactory
import org.gtk.gtk.itemfactory.ListItemFactory.Companion.wrap
import org.gtk.gtk.sorter.Sorter
import org.gtk.gtk.sorter.Sorter.Companion.wrap
import org.gtk.gtk.widgets.ColumnView.Companion.wrap

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ColumnViewColumn.html">
 *     GtkColumnViewColumn</a>
 */
class ColumnViewColumn(
	val columnPointer: CPointer<GtkColumnViewColumn>,
) : KGObject(columnPointer.reinterpret()) {

	constructor(title: String?, factory: ListItemFactory?) :
			this(
				gtk_column_view_column_new(
					title,
					factory?.listItemFactoryPointer
				)!!
			)

	val columnView: ColumnView?
		get() = gtk_column_view_column_get_column_view(columnPointer).wrap()

	var expand: Boolean
		get() = gtk_column_view_column_get_expand(columnPointer).bool
		set(value) = gtk_column_view_column_set_expand(columnPointer, value.gtk)

	var factory: ListItemFactory?
		get() = gtk_column_view_column_get_factory(columnPointer).wrap()
		set(value) = gtk_column_view_column_set_factory(
			columnPointer,
			value?.listItemFactoryPointer
		)

	var fixedWidth: Int
		get() = gtk_column_view_column_get_fixed_width(columnPointer)
		set(value) = gtk_column_view_column_set_fixed_width(
			columnPointer,
			value
		)

	var model: MenuModel?
		get() = gtk_column_view_column_get_header_menu(columnPointer).wrap()
		set(value) = gtk_column_view_column_set_header_menu(
			columnPointer,
			value?.menuModelPointer
		)

	var resizable: Boolean
		get() = gtk_column_view_column_get_resizable(columnPointer).bool
		set(value) = gtk_column_view_column_set_resizable(
			columnPointer,
			value.gtk
		)

	var sorter: Sorter?
		get() = gtk_column_view_column_get_sorter(columnPointer).wrap()
		set(value) = gtk_column_view_column_set_sorter(
			columnPointer,
			value?.sorterPointer
		)

	var title: String?
		get() = gtk_column_view_column_get_title(columnPointer)?.toKString()
		set(value) = gtk_column_view_column_set_title(columnPointer, value)

	var visible: Boolean
		get() = gtk_column_view_column_get_visible(columnPointer).bool
		set(value) = gtk_column_view_column_set_visible(
			columnPointer,
			value.gtk
		)
}
