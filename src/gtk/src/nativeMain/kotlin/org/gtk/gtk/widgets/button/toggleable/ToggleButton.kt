package org.gtk.gtk.widgets.button.toggleable

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Actionable
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.button.Button

/**
 * kotlinx-gtk
 *
 * 16 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ToggleButton.html">
 *     GtkToggleButton</a>
 */
open class ToggleButton(
	val toggleButtonPointer: GtkToggleButton_autoptr,
) : Button(toggleButtonPointer.reinterpret()), Actionable {

	override val actionablePointer: GtkActionable_autoptr by lazy {
		toggleButtonPointer.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_TOGGLE_BUTTON))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ToggleButton.new.html">
	 *     gtk_toggle_button_new</a>
	 */
	constructor() : this(gtk_toggle_button_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ToggleButton.new_with_label.html">
	 *     gtk_toggle_button_new_with_label</a>
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ToggleButton.new_with_mnemonic.html">
	 *     gtk_toggle_button_new_with_mnemonic</a>
	 */
	constructor(label: String, mnemonic: Boolean = false) : this(
		(if (mnemonic)
			gtk_toggle_button_new_with_label(
				label
			)
		else gtk_toggle_button_new_with_mnemonic(
			label
		))!!.reinterpret()
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ToggleButton.get_active.html">
	 *     gtk_toggle_button_get_active</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ToggleButton.set_active.html">
	 *     gtk_toggle_button_set_active</a>
	 */
	var active: Boolean
		get() = gtk_toggle_button_get_active(toggleButtonPointer).bool
		set(value) = gtk_toggle_button_set_active(
			toggleButtonPointer,
			value.gtk
		)

	fun setGroup(group: ToggleButton?) {
		gtk_toggle_button_set_group(toggleButtonPointer, group?.toggleButtonPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ToggleButton.toggled.html">
	 *     gtk_toggle_button_toggled</a>
	 */
	fun toggled() {
		gtk_toggle_button_toggled(toggleButtonPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ToggleButton.toggled.html">
	 *     toggled</a>
	 */
	fun addOnToggledCallback(action: TypedNoArgFunc<ToggleButton>): SignalManager =
		addSignalCallback(Signals.TOGGLED, action, staticNoArgGCallback)

	companion object {
		inline fun GtkToggleButton_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkToggleButton_autoptr.wrap() =
			ToggleButton(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkToggleButton_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<ToggleButton>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}