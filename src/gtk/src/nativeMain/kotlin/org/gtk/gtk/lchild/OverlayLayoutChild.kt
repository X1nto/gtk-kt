package org.gtk.gtk.lchild

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/method.OverlayLayoutChild.get_measure.html">
 *     GtkOverlayLayoutChild</a>
 */
class OverlayLayoutChild(
	val overlayPointer: GtkOverlayLayoutChild_autoptr,
) : LayoutChild(overlayPointer.reinterpret()) {

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_OVERLAY_LAYOUT_CHILD
	))

	var clipOverlay: Boolean
		get() = gtk_overlay_layout_child_get_clip_overlay(overlayPointer).bool
		set(value) = gtk_overlay_layout_child_set_clip_overlay(
			overlayPointer,
			value.gtk
		)

	var measure: Boolean
		get() = gtk_overlay_layout_child_get_measure(overlayPointer).bool
		set(value) = gtk_overlay_layout_child_set_measure(
			overlayPointer,
			value.gtk
		)
}