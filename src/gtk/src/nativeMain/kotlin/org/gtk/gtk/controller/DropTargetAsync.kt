package org.gtk.gtk.controller

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.ContentFormats
import org.gtk.gdk.ContentFormats.Companion.wrap
import org.gtk.gdk.Drop
import org.gtk.gdk.Drop.Companion.wrap
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DropTargetAsync.html">
 *     GtkDropTargetAsync</a>
 */
class DropTargetAsync(
	val targetPointer: CPointer<GtkDropTargetAsync>,
) : EventController(targetPointer.reinterpret()) {

	constructor(type: ContentFormats, actions: GdkDragAction) :
			this(gtk_drop_target_async_new(type.structPointer, actions)!!)

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_DROP_TARGET_ASYNC
	))

	var actions: GdkDragAction
		get() = gtk_drop_target_async_get_actions(targetPointer)
		set(value) = gtk_drop_target_async_set_actions(targetPointer, value)

	var formats: ContentFormats?
		get() = gtk_drop_target_async_get_formats(targetPointer).wrap()
		set(value) = gtk_drop_target_async_set_formats(
			targetPointer,
			value?.structPointer
		)

	fun rejectDrop(drop: Drop) {
		gtk_drop_target_async_reject_drop(targetPointer, drop.dropPointer)
	}

	fun addOnAcceptCallback(action: DropTargetAsyncAcceptFunc) =
		addSignalCallback(Signals.ACCEPT, action, staticAcceptFunc)

	fun addOnDragEnterCallback(action: DropTargetAsyncXYForDragFunc) =
		addSignalCallback(Signals.ENTER, action, staticXYFunc)

	fun addOnDragLeaveCallback(action: DropTargetAsyncLeaveFunc) =
		addSignalCallback(Signals.LEAVE, action, staticLeaveFunc)

	fun addOnDragMotionCallback(action: DropTargetAsyncXYForDragFunc) =
		addSignalCallback(Signals.MOTION, action, staticXYFunc)

	fun addOnDropCallback(action: DropTargetAsyncDropFunc) =
		addSignalCallback(Signals.DROP, action, staticDropFunc)

	companion object {
		inline fun CPointer<GtkDropTargetAsync>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkDropTargetAsync>.wrap() =
			DropTargetAsync(this)


		private val staticAcceptFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTargetAsync>,
					drop: GdkDrop_autoptr,
					data: gpointer,
				->
				data.asStableRef<DropTargetAsyncAcceptFunc>()
					.get()
					.invoke(self.wrap(), drop.wrap())
					.gtk
			}.reinterpret()

		private val staticXYFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTargetAsync>,
					drop: GdkDrop_autoptr,
					x: Double,
					y: Double,
					data: gpointer,
				->
				data.asStableRef<DropTargetAsyncXYForDragFunc>()
					.get()
					.invoke(self.wrap(), drop.wrap(), x, y)
			}.reinterpret()

		private val staticLeaveFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTargetAsync>,
					drop: GdkDrop_autoptr,
					data: gpointer,
				->
				data.asStableRef<DropTargetAsyncLeaveFunc>()
					.get()
					.invoke(self.wrap(), drop.wrap())
			}.reinterpret()

		private val staticDropFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTargetAsync>,
					drop: GdkDrop_autoptr,
					x: Double,
					y: Double,
					data: gpointer,
				->
				data.asStableRef<DropTargetAsyncDropFunc>()
					.get()
					.invoke(self.wrap(), drop.wrap(), x, y)
					.gtk
			}.reinterpret()
	}
}

typealias DropTargetAsyncAcceptFunc =
		DropTargetAsync.(Drop) -> Boolean

typealias DropTargetAsyncXYForDragFunc =
		DropTargetAsync.(Drop, x: Double, y: Double) -> GdkDragAction

typealias DropTargetAsyncLeaveFunc =
		DropTargetAsync.(Drop) -> Unit

typealias DropTargetAsyncDropFunc =
		DropTargetAsync.(Drop, x: Double, y: Double) -> Boolean



