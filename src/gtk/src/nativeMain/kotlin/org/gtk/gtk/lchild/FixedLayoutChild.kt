package org.gtk.gtk.lchild

import gtk.GTK_TYPE_FIXED_LAYOUT_CHILD
import gtk.GtkFixedLayoutChild_autoptr
import gtk.gtk_fixed_layout_child_get_transform
import gtk.gtk_fixed_layout_child_set_transform
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gsk.Transform
import org.gtk.gsk.Transform.Companion.wrap

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FixedLayoutChild.html">
 *     GtkFixedLayoutChild</a>
 */
class FixedLayoutChild(
	val fixedPointer: GtkFixedLayoutChild_autoptr,
) : LayoutChild(fixedPointer.reinterpret()) {

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_FIXED_LAYOUT_CHILD
	))

	var transform: Transform?
		get() = gtk_fixed_layout_child_get_transform(fixedPointer).wrap()
		set(value) = gtk_fixed_layout_child_set_transform(
			fixedPointer,
			value?.transformPointer
		)
}