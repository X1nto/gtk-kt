package org.gtk.gsk

import gtk.GskGLShader
import kotlinx.cinterop.CPointer

/**
 * kotlinx-gtk
 *
 * 27 / 10 / 2021
 *
 * @see <a href=""></a>
 */
class GLShader(val glShaderPointer: CPointer<GskGLShader>) {
}