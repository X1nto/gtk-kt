package org.gtk.gsk

import gtk.GskTransform_autoptr
import gtk.gsk_transform_new

/**
 * kotlinx-gtk
 *
 * 24 / 07 / 2021
 *
 * @see <a href="https://docs.gtk.org/gsk4/struct.Transform.html">
 *     GskTransform</a>
 */
class Transform(
	val transformPointer: GskTransform_autoptr,
) {

	constructor() : this(gsk_transform_new()!!)

	companion object {
		inline fun GskTransform_autoptr?.wrap() =
			this?.wrap()

		inline fun GskTransform_autoptr.wrap() =
			Transform(this)
	}
}