package org.gtk.gdk

import gtk.GdkDrawContext_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/class.DrawContext.html">
 *     GdkDrawContext</a>
 */
open class DrawContext(
	val drawContextPointer: GdkDrawContext_autoptr,
) : KGObject(drawContextPointer.reinterpret()) {
}