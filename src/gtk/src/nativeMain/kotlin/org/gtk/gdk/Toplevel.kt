package org.gtk.gdk

import gtk.GdkToplevel_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 18 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/iface.Toplevel.html">
 *     GdkToplevel</a>
 */
class Toplevel(
	val topLevelPointer: GdkToplevel_autoptr,
) : KGObject(topLevelPointer.reinterpret()) {

	companion object {
		inline fun GdkToplevel_autoptr?.wrap() =
			this?.wrap()

		inline fun GdkToplevel_autoptr.wrap() =
			Toplevel(this)
	}
}