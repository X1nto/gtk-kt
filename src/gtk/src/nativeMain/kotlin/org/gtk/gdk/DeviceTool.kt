package org.gtk.gdk

import gtk.*
import gtk.GdkDeviceToolType.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gdk4/class.DeviceTool.html">
 *     GdkDeviceTool</a>
 */
class DeviceTool(
	val deviceToolPointer: CPointer<GdkDeviceTool>,
) : KGObject(deviceToolPointer.reinterpret()) {

	val axes: GdkAxisFlags
		get() = gdk_device_tool_get_axes(deviceToolPointer)

	val hardwareId: ULong
		get() = gdk_device_tool_get_hardware_id(deviceToolPointer)

	val serial: ULong
		get() = gdk_device_tool_get_serial(deviceToolPointer)

	val type: Type
		get() = Type.valueOf(gdk_device_tool_get_tool_type(deviceToolPointer))

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/enum.DeviceToolType.html">
	 *     GdkDeviceToolType</a>
	 */
	enum class Type(
		val gdk: GdkDeviceToolType,
	) {
		UNKNOWN(GDK_DEVICE_TOOL_TYPE_UNKNOWN),
		PEN(GDK_DEVICE_TOOL_TYPE_PEN),
		ERASER(GDK_DEVICE_TOOL_TYPE_ERASER),
		BRUSH(GDK_DEVICE_TOOL_TYPE_BRUSH),
		PENCIL(GDK_DEVICE_TOOL_TYPE_PENCIL),
		AIRBRUSH(GDK_DEVICE_TOOL_TYPE_AIRBRUSH),
		MOUSE(GDK_DEVICE_TOOL_TYPE_MOUSE),
		LENS(GDK_DEVICE_TOOL_TYPE_LENS);

		companion object {
			fun valueOf(gdk: GdkDeviceToolType) =
				when (gdk) {
					GDK_DEVICE_TOOL_TYPE_UNKNOWN -> UNKNOWN
					GDK_DEVICE_TOOL_TYPE_PEN -> PEN
					GDK_DEVICE_TOOL_TYPE_ERASER -> ERASER
					GDK_DEVICE_TOOL_TYPE_BRUSH -> BRUSH
					GDK_DEVICE_TOOL_TYPE_PENCIL -> PENCIL
					GDK_DEVICE_TOOL_TYPE_AIRBRUSH -> AIRBRUSH
					GDK_DEVICE_TOOL_TYPE_MOUSE -> MOUSE
					GDK_DEVICE_TOOL_TYPE_LENS -> LENS
				}
		}
	}

	companion object {

		inline fun CPointer<GdkDeviceTool>?.wrap() =
			this?.wrap()

		inline fun CPointer<GdkDeviceTool>.wrap() =
			DeviceTool(this)
	}
}