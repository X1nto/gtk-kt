package org.gtk.graphene

import gtk.graphene_vec4_t
import kotlinx.cinterop.CPointer

/**
 * kotlinx-gtk
 *
 * 27 / 10 / 2021
 *
 * @see <a href=""></a>
 */
class Vec4(val vec4Pointer: CPointer<graphene_vec4_t>) {
}