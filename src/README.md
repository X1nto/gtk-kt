# SRC

This directory contains the many modules that gtk-kt depends on.


## Modules
- [Cairo](./cairo/README.md)
- [GDKPixBuf](./gdk-pixbuf/README.md)
- [GIO](./gio/README.md)
- [GLib](./glib/README.md)
- [GObject](./gobject/README.md)
- [GTK](./gtk/README.md)
- [Pango](./pango/README.md)