package org.gtk.glib.ktx

import kotlinx.cinterop.*
import org.gtk.glib.SList
import org.gtk.glib.WrappedSList

/*
 * kotlinx-gtk
 *
 * 21 / 08 / 2021
 */

/**
 * Easy way to define a string list
 */
fun WrappedStringSList(memScope: AutofreeScope, sList: SList = SList()) =
	WrappedSList(
		sList = sList,
		wrapPointer = { reinterpret<ByteVarOf<Byte>>().toKString() },
		{ cstr.getPointer(memScope) }
	)