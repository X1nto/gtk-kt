package org.gtk.gtk.ktx

import org.gtk.gtk.KGBitSet

inline operator fun KGBitSet.plus(value: UInt) =
	add(value)