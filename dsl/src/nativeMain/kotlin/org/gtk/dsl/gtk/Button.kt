package org.gtk.dsl.gtk

import org.gtk.dsl.GtkDsl
import org.gtk.gtk.widgets.Grid
import org.gtk.gtk.widgets.box.Box
import org.gtk.gtk.widgets.button.Button
import org.gtk.gtk.widgets.frame.Frame
import org.gtk.gtk.widgets.windows.Window

@GtkDsl
inline fun Box.button(
	label: String? = null,
	append: Boolean = true,
	buttonBuilder: Button .() -> Unit = {}
) = (label?.let(::Button) ?: Button()).apply(buttonBuilder).also { if (append) append(it) else prepend(it) }

@GtkDsl
inline fun Frame.button(
	label: String? = null,
	buttonBuilder: Button .() -> Unit = {}
) = (label?.let(::Button) ?: Button()).apply(buttonBuilder).also { child = it }

@GtkDsl
inline fun Window.button(
	label: String? = null,
	buttonBuilder: Button .() -> Unit = {}
) = (label?.let(::Button) ?: Button()).apply(buttonBuilder).also { child = it }

@GtkDsl
inline fun Grid.button(
	label: String?,
	x: Int,
	y: Int,
	width: Int,
	height: Int,
	buttonBuilder: Button.() -> Unit = {}
) = (label?.let(::Button) ?: Button()).apply(buttonBuilder).also {
	attach(it, x, y, width, height)
}

@GtkDsl
inline fun Grid.button(
	x: Int,
	y: Int,
	width: Int,
	height: Int,
	label: String? = null,
	buttonBuilder: Button.() -> Unit = {}
) = (label?.let(::Button) ?: Button()).apply(buttonBuilder).also {
	attach(it, x, y, width, height)
}


@GtkDsl
inline fun Button.onClicked(crossinline action: () -> Unit) = addOnClickedCallback { action() }