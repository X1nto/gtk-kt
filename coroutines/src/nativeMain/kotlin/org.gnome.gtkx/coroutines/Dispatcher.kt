package org.gnome.gtkx.coroutines

import glib.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import kotlinx.coroutines.*
import kotlinx.coroutines.internal.MainDispatcherFactory
import kotlinx.datetime.Clock.System
import org.gtk.glib.asStablePointer
import org.gtk.gobject.staticDestroyStableRefFunction
import kotlin.coroutines.CoroutineContext

/**
 * Run on the main loop
 */
val Dispatchers.UI: CoroutineDispatcher
	get() = GTK

/**
 * Run on the main loop
 */
val Dispatchers.GTK: CoroutineDispatcher
	get() = org.gnome.gtkx.coroutines.GTK

private val stack: ArrayList<Pair<CoroutineContext, Runnable>> by lazy {
	arrayListOf()
}

private val idle_function: GSourceFunc = staticCFunction { data: gpointer? ->
	data?.asStableRef<Runnable>()?.get()?.run()
	G_SOURCE_REMOVE
}

@OptIn(InternalCoroutinesApi::class)
public sealed class GTKDispatcher : MainCoroutineDispatcher(), Delay {
	override fun dispatch(context: CoroutineContext, block: Runnable) {
		g_idle_add_full(
			G_PRIORITY_DEFAULT,
			idle_function,
			block.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}


	/** @suppress */
	@OptIn(ExperimentalCoroutinesApi::class)
	override fun scheduleResumeAfterDelay(
		timeMillis: Long,
		continuation: CancellableContinuation<Unit>,
	) {
		val delayedAction = schedule(timeMillis) {
			with(continuation) { resumeUndispatched(Unit) }
		}
		continuation.invokeOnCancellation { delayedAction.stop() }
	}

	/** @suppress */
	override fun invokeOnTimeout(
		timeMillis: Long,
		block: Runnable,
		context: CoroutineContext,
	): DisposableHandle {
		val delayedAction = schedule(timeMillis) {
			block.run()
		}
		return DisposableHandle { delayedAction.stop() }
	}


	private fun schedule(timeMillis: Long, action: () -> Unit): DelayedAction =
		DelayedAction(timeMillis, action).apply {
			start()
		}

	/**
	 * Delayed action that occurs on a separate GThread.
	 */
	private class DelayedAction(timeMillis: Long, action: () -> Unit) {
		fun getTimeMS() =
			System.now().toEpochMilliseconds()

		var startTime = getTimeMS()
		val thread: GThread_autoptr
		var isStarted = false
		val threadAction: () -> Unit = {
			// Wait till told to start
			while (!isStarted) {
				g_thread_yield()
			}

			// Wait till time to start
			while (startTime + timeMillis < getTimeMS()) {
				g_thread_yield()
			}

			action()
		}
		val stablePointer = threadAction.asStablePointer()


		init {
			thread = g_thread_new("timer", staticThreadFunc, stablePointer)!!
		}

		fun start() {
			isStarted = true
		}

		fun stop() {
			// Make sure thread has completed without invoking the start
			stablePointer.asStableRef<() -> Unit>().dispose()
			startTime = getTimeMS()
			isStarted = true
		}

		companion object {
			private val staticThreadFunc: GThreadFunc = staticCFunction { data: gpointer? ->
				data?.asStableRef<() -> Unit>()?.get()?.invoke()
				null
			}
		}
	}
}

@OptIn(InternalCoroutinesApi::class)
internal class GTKDispatcherFactory : MainDispatcherFactory {
	override val loadPriority: Int
		get() = 0

	override fun createDispatcher(allFactories: List<MainDispatcherFactory>): MainCoroutineDispatcher =
		GTK
}

private object ImmediateGTKDispatcher : GTKDispatcher() {
	override val immediate: MainCoroutineDispatcher
		get() = this

	override fun isDispatchNeeded(context: CoroutineContext): Boolean {
		return super.isDispatchNeeded(context)
	}

}

internal object GTK : GTKDispatcher() {
	override val immediate: MainCoroutineDispatcher
		get() = ImmediateGTKDispatcher

	@OptIn(InternalCoroutinesApi::class)
	override fun toString(): String = toStringInternalImpl() ?: "GTK"
}


/**
 * Defines the IO thread
 * All events interacting with IO streams should occur here
 */
@Suppress("unused")
@ExperimentalCoroutinesApi
val Dispatchers.IO: CoroutineDispatcher
	get() = newSingleThreadContext("IO")

@DelicateCoroutinesApi
inline fun launchUI(crossinline block: suspend CoroutineScope.() -> Unit) =
	GlobalScope.launch(context = Dispatchers.UI) {
		block()
	}

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
inline fun launchIO(crossinline block: suspend CoroutineScope.() -> Unit) =
	GlobalScope.launch(context = Dispatchers.IO) {
		block()
	}

@DelicateCoroutinesApi
inline fun launchDefault(crossinline block: suspend CoroutineScope.() -> Unit) =
	GlobalScope.launch(context = Dispatchers.Default) {
		block()
	}

@DelicateCoroutinesApi
inline fun launchUnconfined(crossinline block: suspend CoroutineScope.() -> Unit) =
	GlobalScope.launch(context = Dispatchers.Unconfined) {
		block()
	}

